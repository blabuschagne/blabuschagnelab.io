import { shallowMount } from "@vue/test-utils";
import Component from "../../docs/.vuepress/components/nav_toolbar.vue";

const dummyURL = "wfeiongnd.rf.ge";

describe("NavToolbar", () => {
  let wrapper;

  beforeEach(() => {
    wrapper = shallowMount(Component);
  });

  describe("Navigtion", () => {
    let navigationItems;

    beforeEach(() => {
      navigationItems = wrapper.find("ul");
    });

    it("renders the avatar element", () => {
      expect(navigationItems).toBeTruthy();
    });
  });
});
