module.exports = {
  title: "Brandon Labuschagne",
  description: "Vue-powered static site generator running on GitLab Pages",
  base: "/",
  dest: "public"
};
