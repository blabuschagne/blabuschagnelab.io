module.exports = {
  moduleFileExtensions: ['js', 'vue'],
  transform: {
    '^.+\\.vue$': 'vue-jest',
    '^.+\\.(js)?$': 'babel-jest'
  },
  testMatch: [
    '<rootDir>/tests/**/*.(js)'
  ],
  transformIgnorePatterns: ['<rootDir>/node_modules/']
};
